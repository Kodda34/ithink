
from django.shortcuts import redirect, render
from .models import Usuarios, Product, Comentario
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers

#login
def sign(request):
    listatotal = Usuarios.objects.all() 
    userjson = serializers.serialize("json", listatotal, cls=DjangoJSONEncoder)
    return render(request, 'SignIN.html', {'userjson':userjson})

def rgstr(request):
    if  request.method == "POST":
        data = request.POST
        Usuarios.objects.create(
            
            email = data['email'],
            name = data['nombre'],
            lname = data['apellidos'],
            contrasena = data['password']

        )
        
        return redirect('http://127.0.0.1:8000')
    return render(request, 'rgstr.html')

#Home
def home(request, id):
    user = Usuarios.objects.get(id=id)
    listatotal = Product.objects.all()
    lista = []
    for x in range(0,len(listatotal)):
        CostoMeses = "{:.2f}".format( int(listatotal[x].precio) / 12 )
        meses = '12x $ ' + CostoMeses + ' sin intereses'   
        precio = '$' + listatotal[x].precio + '.00'
        lista.append({'imagen1': listatotal[x].imagen1, 'id': listatotal[x].id , 'uid': user.id,'nombre': listatotal[x].nombre,'precio': precio, 'meses': meses, 'envio': 'Envio gratis'})

    return render(request, 'home.html', {'lista' : lista, 'uid': user.id, 'email': user.email, 'name': user.name, 'lname': user.lname, 'admin': user.admin})
    
def remove(request,id,pid):
    Product.objects.filter(id=pid).delete()
    return render(request, 'remove.html', {'id':id})

def addProduct(request,id):
    if  request.method == "POST":
        data = request.POST
        prod = Product.objects.create(

            imagen1 = request.FILES.get('myFile'),
            imagen2 = request.FILES.get('myFile2'),
            imagen3 = request.FILES.get('myFile3'),
            nombre = data['nombre'],
            precio = data['precio'],
            descripcion = data['descripcion']

        )
        
        return redirect('http://127.0.0.1:8000/home/id='+str(id))

    return render(request, 'addProduct.html')


def editProduct(request, id, pid):

    prod = Product.objects.get(id=pid)

    if  request.method == "POST":
        data = request.POST
        prod.nombre = data['nombre']
        prod.precio = data['precio']
        prod.descripcion = data['descripcion']

        prod.save()
        
        return redirect("http://127.0.0.1:8000/home/id="+str(id))

    return render(request, 'editProduct.html', {'imagen1': prod.imagen1,'imagen2': prod.imagen2,'imagen3': prod.imagen3,'nombre': prod.nombre,'precio': prod.precio,'descripcion': prod.descripcion})

#Products  
def product(request,id,pid):
    user = Usuarios.objects.get(id=id)
    if  request.method == "POST":
        data = request.POST
        if('Comentario' in data):
            Comentario.objects.create(
                pid = pid,
                nombre = user.name,
                comentar = data['Comentario']
            )
        else:
            comid=Comentario.objects.get(id=data['cid'])
            comid.comentar = data['txtactualizar']
            comid.save()
        return redirect('http://127.0.0.1:8000/product/id='+str(id)+'&pid='+str(pid))

    producto = Product.objects.get(id=pid)
    comentarios = Comentario.objects.all().filter(pid = pid)
    CostoMeses = "{:.2f}".format( int(producto.precio) / 12 )
    meses = '12x $ ' + CostoMeses + ' sin intereses' 
    precio = '$' + producto.precio + '.00'
    lista = []
    for x in range(0,len(comentarios)):
        lista.append({ 'cid': comentarios[x].id, 'nombre': comentarios[x].nombre, 'comentar' : comentarios[x].comentar})

    return render(request, 'product.html' ,{ 'lista': lista, 'uid': user.id, 'pid': producto.id, 'imagen1': producto.imagen1, 'imagen2':producto.imagen2, 'imagen3': producto.imagen3, 'meses': meses, 'precio' : precio, 'nombre': producto.nombre, 'descripcion': producto.descripcion, 'admin': user.admin})

def removeComent(request, uid, cid, pid):
    Comentario.objects.filter(id=cid).delete()
    return render(request, 'removeCom.html',{'uid':uid, 'pid':pid})



