from django.contrib import admin
from .models import Comentario, Usuarios, Product

admin.site.register(Usuarios)

admin.site.register(Product)

admin.site.register(Comentario)