from django.db import models
from django.utils import timezone

class Usuarios(models.Model):
    email = models.CharField(max_length=100)
    name = models.CharField(max_length=50, default='No name')
    lname = models.CharField(max_length=50, default='No name')
    contrasena = models.CharField(max_length=15)
    admin = models.BooleanField(default=False)
    daycreated = models.DateTimeField(default=timezone.now)
    
class Product(models.Model):
    imagen1 = models.ImageField(upload_to='media', default='NOIMAGE.png')
    imagen2 = models.ImageField(upload_to='media', default='NOIMAGE.png')
    imagen3 = models.ImageField(upload_to='media', default='NOIMAGE.png')
    nombre = models.CharField(max_length=100)
    precio = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=1000)

class Comentario(models.Model):
    pid = models.CharField(max_length=10, default='404')
    nombre = models.CharField(max_length=30)
    comentar = models.CharField(max_length=100)