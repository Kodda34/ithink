# Generated by Django 3.2.4 on 2021-06-28 03:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iThink', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('imagen', models.CharField(max_length=100)),
                ('nombre', models.CharField(max_length=100)),
                ('precio', models.CharField(max_length=10)),
                ('descripcion', models.CharField(max_length=1000)),
            ],
        ),
    ]
