# Generated by Django 3.2.4 on 2021-06-28 06:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('iThink', '0005_alter_product_imagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='imagen2',
            field=models.ImageField(default='NOIMAGE.png', upload_to='media'),
        ),
        migrations.AddField(
            model_name='product',
            name='imagen3',
            field=models.ImageField(default='NOIMAGE.png', upload_to='media'),
        ),
        migrations.AlterField(
            model_name='product',
            name='imagen',
            field=models.ImageField(default='NOIMAGE.png', upload_to='media'),
        ),
    ]
