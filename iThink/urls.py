from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    #Registro
    path('', views.sign),
    path('signIN/', views.sign),
    path('rgstr/', views.rgstr),

    #Home
    path('home/id=<int:id>', views.home , name='home'),
    path('product/remove/id=<int:id>&pid=<int:pid>', views.remove),
    path('product/edit/id=<int:id>&pid=<int:pid>', views.editProduct),
    path('product/add/id=<int:id>',views.addProduct),

    #Product
    path('product/id=<int:id>&pid=<int:pid>', views.product),
    path('coment/remove/uid=<int:uid>&cid=<int:cid>&pid=<int:pid>', views.removeComent),

    #Database
    path('admin/', admin.site.urls),
 
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
